import { firebaseHelper } from "../helper/firebaseHelper.js";
import { homeController } from "./controller/homeController.js";
import { loginController } from "./controller/loginController.js";

if (navigator.serviceWorker) {
	navigator.serviceWorker.register("../sw.js");

	firebaseHelper();

	let usuario = JSON.parse(localStorage.getItem("usuario"));

	if (usuario != null) {
		homeController();
	} else {
		loginController();
	}
}

//JSON.parse(localStorage.getItem("usuario"));
//localStorage.setItem("usuario", JSON.stringify({usuario:'jesus'}));
