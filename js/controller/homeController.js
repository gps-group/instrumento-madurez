import { loginController } from "./loginController.js";
import { madurezController } from "./madurezController.js";
import { getMadurez } from "../model/homeModel.js";
import { homeView } from "../view/homeView.js";
import { saveMadurez, saveMadurez2 } from "../model/madurezModel.js";
import {} from "../../lib/chart.js";

let body;
let madurezCanva;
let areasCanva;
let contador;
let cerrarSesionButton;
let medicionButton;
let empresaH1;

export const homeController = () => {
	body = document.body;
	body.innerHTML = homeView();
	//
	madurezCanva = document.getElementById("madurezCanva");
	areasCanva = document.getElementById("areasCanva");
	cerrarSesionButton = document.getElementById("cerrarSesionButton");
	medicionButton = document.getElementById("medicionButton");
	//
	empresaH1 = document.getElementById("empresaH1");
	//
	//
	cerrarSesionButton.addEventListener("click", () => {
		localStorage.setItem("usuario", JSON.stringify(null));
		loginController();
	});
	medicionButton.addEventListener("click", () => {
		madurezController();
	});
	let usuario = JSON.parse(localStorage.getItem("usuario"));
	empresaH1.innerHTML = `${usuario.nombreEmpresa}`;
	if (usuario.primerRegistro == true) {
		madurezController();
	}

	// let madurezSave = JSON.parse(localStorage.getItem("maduerezSave"));
	// console.log(madurezSave);

	// if (madurezSave != null) {
	// 	saveMadurez2(madurezSave);
	// }

	getMadurez(usuario.uid);
	//
	//
};

export const getDatos = (params) => {
	if (params == null) {
		params = JSON.parse(localStorage.getItem("madurez"));
	} else {
	}
	console.log(params);

	let d1 = (params.dimensiones[0].valor / 15) * 100;
	let d2 = (params.dimensiones[1].valor / 32) * 100;
	let d3 = (params.dimensiones[2].valor / 16) * 100;
	let d4 = (params.dimensiones[3].valor / 13) * 100;
	let d5 = (params.dimensiones[4].valor / 24) * 100;
	//
	new Chart(madurezCanva, {
		type: "bar",
		data: {
			labels: ["Porcentaje"],
			datasets: [
				{
					label: "Madurez Digital",
					data: [params.total, 100],
					lineTension: 10,
					backgroundColor: "#001540",
					borderColor: "#001540",
					borderWidth: 0,
					pointBackgroundColor: "#001540",
				},
			],
		},
	});
	new Chart(areasCanva, {
		type: "radar",
		data: {
			labels: [
				"Automatización de operaciones",
				"Comercio digital",
				"Logística",
				"Sistemas de pago",
				"Marca",
			],
			datasets: [
				{
					label: "Áreas de oportunidad",
					// 15,32,16,13,24
					data: [
						Math.round(d1),
						Math.round(d2),
						Math.round(d3),
						Math.round(d4),
						Math.round(d5),
					],
					lineTension: 0.01,
					backgroundColor: "#001540",
					borderColor: "#001540",
					borderWidth: 0,
				},
			],
		},
		// options: {
		// 	scale: {
		// 		ticks: {
		// 			min: 0,
		// 			max: 100,
		// 			suggestedMin: 0,
		// 			suggestedMax: 100,
		// 			maxTicksLimit: 1,
		// 		},
		// 	},
		// },
	});
};

// document.getElementById("divTabla1").innerHTML = `
//                                 <canvas class="my-4 w-100" id="tabla1" width="900" height="380"></canvas>
//                               `;

//tabla1(total.dia1, total.dia2, total.dia3, total.dia4, total.dia5, total.dia6, total.dia7);

// const tabla1 = (d1, d2, d3, d4, d5, d6, d7) => {
// 	new Chart(document.getElementById("tabla1"), {
// 		type: "bar",
// 		data: {
// 			labels: [`${getFecha(-6)}`, `${getFecha(-5)}`, `${getFecha(-4)}`, `${getFecha(-3)}`, `${getFecha(-2)}`, `${getFecha(-1)}`, `${getFecha(0)}`],
// 			datasets: [
// 				{
// 					label: "Ventas - Últimos 7 días",
// 					data: [d1, d2, d3, d4, d5, d6, d7],
// 					lineTension: 0.15,
// 					backgroundColor: "#950101",
// 					borderColor: "#3d0000",
// 					borderWidth: 3,
// 					pointBackgroundColor: "white",
// 				},
// 			],
// 		},
// 	});
// };
