import { iniciaSesionController } from "./inicioSesionController.js";
import { crearUsuarioModel } from "../model/registroModel.js";
import { registroView } from "../view/registroView.js";

let body;
let iniciaSesionButton;
let registroForm;
let params;
let modalFooterDiv;
let modalBodyDiv;

export const registroController = () => {
	body = document.body;
	body.innerHTML = registroView();
	//
	iniciaSesionButton = document.getElementById("iniciaSesionButton");
	registroForm = document.getElementById("registroForm");
	modalFooterDiv = document.getElementById("modalFooterDiv");
	modalBodyDiv = document.getElementById("modalBodyDiv");
	//
	iniciaSesionButton.addEventListener("click", () => {
		iniciaSesionController();
	});
	registroForm.addEventListener("submit", (e) => {
		e.preventDefault();
		params = Object.fromEntries(new FormData(e.target).entries());
		//
		if (params.nombreEmpresa.length < 1) {
			modalBodyDiv.innerHTML = "⚠ Falta el nombre de la empresa";
			return;
		}
		if (params.correo.length < 1) {
			modalBodyDiv.innerHTML = "⚠ Falta el correo";
			return;
		}
		if (!params.correo.includes("@")) {
			modalBodyDiv.innerHTML = "⚠ Correo no válido";
			return;
		}
		if (!params.correo.includes(".")) {
			modalBodyDiv.innerHTML = "⚠ Correo no válido";
			return;
		}
		if (params.contrasena.length < 1) {
			modalBodyDiv.innerHTML = "⚠ Falta la contraseña";
			return;
		}
		if (params.contrasena.length < 8) {
			modalBodyDiv.innerHTML = "⚠ Contraseña muy corta";
			return;
		}
		if (params.contrasena != params.confirmarContrasena) {
			modalBodyDiv.innerHTML = "⚠ Las contraseñas no coinciden";
			return;
		}
		modalFooterDiv.style.display = "none";
		modalBodyDiv.innerHTML = `
            <center>
                <div class="spinner-border spinnerDiv" role="status"></div>
            </center>
        `;
		//
		crearUsuarioModel(params);
	});
};
