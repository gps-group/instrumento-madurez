import { iniciaSesionController } from "./inicioSesionController.js";
import { registroController } from "./registroController.js";
import { loginView } from "../view/loginView.js";

let body;
let iniciaSesionButton;
let registrateButton;

export const loginController = () => {
	body = document.body;
	body.innerHTML = loginView();
	//
	iniciaSesionButton = document.getElementById("iniciaSesionButton");
	registrateButton = document.getElementById("registrateButton");
	//
	iniciaSesionButton.addEventListener("click", () => {
		iniciaSesionController();
	});
	registrateButton.addEventListener("click", () => {
		registroController();
	});
};
