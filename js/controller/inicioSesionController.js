import { registroController } from "./registroController.js";
import { loginModel } from "../model/inicioSesionModel.js";
import { iniciaSesionView } from "../view/inicioSesionView.js";

let body;
let registrateButton;
let inicioSesionForm;
let spinnerDiv;
let iniciarInput;

export const iniciaSesionController = () => {
	body = document.body;
	body.innerHTML = iniciaSesionView();
	//
	registrateButton = document.getElementById("registrateButton");
	inicioSesionForm = document.getElementById("inicioSesionForm");
	spinnerDiv = document.getElementById("spinnerDiv");
	iniciarInput = document.getElementById("iniciarInput");
	//
	spinnerDiv.style.display = "none";
	//
	registrateButton.addEventListener("click", () => {
		registroController();
	});
	//
	inicioSesionForm.addEventListener("submit", (e) => {
		e.preventDefault();
		let params = Object.fromEntries(new FormData(e.target).entries());
		iniciarInput.style.display = "none";
		spinnerDiv.style.display = "block";
		loginModel(params, iniciarInput, spinnerDiv);
	});
};
