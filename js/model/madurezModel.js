import { homeController } from "../controller/homeController.js";

export const saveMadurez = (params) => {
	// localStorage.setItem(
	// 	"madurezSave",
	// 	JSON.stringify({
	// 		uid: params.uid,
	// 		total: params.total,
	// 		dimensiones: params.dimensiones,
	// 		//createdAt: firebase.firestore.FieldValue.serverTimestamp(),
	// 	})
	// );

	// localStorage.setItem(
	// 	"madurez",
	// 	JSON.stringify({
	// 		uid: params.uid,
	// 		total: params.total,
	// 		dimensiones: params.dimensiones,
	// 		//createdAt: firebase.firestore.FieldValue.serverTimestamp(),
	// 	})
	// );

	(async () => {
		console.log("Registrando madurez");

		let doc = await firebase.firestore().collection("madurez").doc().set({
			uid: params.uid,
			total: params.total,
			dimensiones: params.dimensiones,
			createdAt: firebase.firestore.FieldValue.serverTimestamp(),
		});
		console.log("Registrado;");
		let usuario = JSON.parse(localStorage.getItem("usuario"));
		usuario.primerRegistro = false;
		localStorage.setItem("usuario", JSON.stringify(usuario));

		// localStorage.setItem("madurezSave", JSON.stringify(null));

		homeController();

		//
	})().catch((e) => {
		console.log("......................................	");
		console.log(e);
		homeController();
	});
};

export const saveMadurez2 = (params) => {
	localStorage.setItem(
		"madurez",
		JSON.stringify({
			uid: params.uid,
			total: params.total,
			dimensiones: params.dimensiones,
			//createdAt: firebase.firestore.FieldValue.serverTimestamp(),
		})
	);

	(async () => {
		console.log("Registrando madurez save");

		let doc = await firebase.firestore().collection("madurez").doc().set({
			uid: params.uid,
			total: params.total,
			dimensiones: params.dimensiones,
			createdAt: firebase.firestore.FieldValue.serverTimestamp(),
		});
		console.log("Registrado;");
		let usuario = JSON.parse(localStorage.getItem("usuario"));
		usuario.primerRegistro = false;
		localStorage.setItem("usuario", JSON.stringify(usuario));

		localStorage.setItem("madurezSave", JSON.stringify(null));

		//
	})().catch((e) => {
		console.log(e);
	});
};
