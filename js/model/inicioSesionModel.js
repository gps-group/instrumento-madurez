export const loginModel = (params, registrateButton, spinnerDiv) => {
	(async () => {
		let doc1 = await firebase
			.auth()
			.signInWithEmailAndPassword(params.correoInput, params.passwordInput);
		let doc2 = await firebase
			.firestore()
			.collection("usuarios")
			.where("uid", "==", doc1.user.uid)
			.get();
		//
		let nombreEmpresa;
		doc2.forEach((doc) => {
			nombreEmpresa = doc.data().nombreEmpresa;
		});

		localStorage.setItem(
			"usuario",
			JSON.stringify({
				uid: doc1.user.uid,
				correo: params.correoInput,
				nombreEmpresa: nombreEmpresa,
				primerRegistro: false,
			})
		);
		console.log(doc1.user.uid);
		console.log(params.correoInput);
		console.log(nombreEmpresa);
		location.reload();
	})().catch(() => {
		registrateButton.style.display = "block";
		spinnerDiv.style.display = "none";
	});
};
