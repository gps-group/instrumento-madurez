export const madurezView = () => {
	return `
    <link rel="stylesheet" href="/css/madurez.css" />
    <button id="salirButton" class="btn salitButton">Salir❌</button>
    <div id="preguntasDiv" class="preguntasDiv"></div>
    <div id="respuestasDiv" class="respuestasDiv"></div>
    `;
};
