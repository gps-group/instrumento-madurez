export const iniciaSesionView = () => {
	return `
    <link rel="stylesheet" href="/css/inicioSesion.css" />
    <div class="inicioSesionDiv">
        <div class="inicioSesionDiv2">
            <form id="inicioSesionForm" class="inicioSesionForm">
                <input id="usuarioInput" name="correoInput" class="btn" placeholder="Usuario"  />
                <br>
                <br>
                <input id="contrasenaInput" name="passwordInput" type="password" class="btn" placeholder="Contraseña" />
                <br>
                <br>
                <center>
                    <input id="iniciarInput" class="btn" type="submit" value="Iniciar" />
                </center>
            </form>
            <center>
                <button id="registrateButton" class="btn registrateInput">Regístrate</button>
            </center>
            <div id="spinnerDiv">
                <center>
			        <div class="spinner-border"></div>
		        </center>
            </div>
        </div>
    </div>
    `;
};
